package com.polyplanes.components;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Classement {
    String identifiant;
    String name;
    Integer value;
}
