package com.polyplanes.components.companyAnalyseList;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.ArrayList;

@Document(collection = "companies")
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CompanyAnalyseList {
    @Id
    String id;
    ArrayList<CompanyAnalyse> DATA;
    String idCarrier;
    String name;
}
