package com.polyplanes.components.companyAnalyseList;

import com.polyplanes.components.Classement;
import lombok.*;

import java.util.ArrayList;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CompanyAnalyse {
    Integer nbCanceledFlights;
    Integer nbFlights;
    Integer nbDelayedFlights;
    Float avgDelayedCanceledFlights;
    ArrayList<Classement> topDelayReason;
    ArrayList<Classement> topCancelReason;
    Float avgElapsedTime;
    ArrayList<Integer> topByCatElapsedTime;
    ArrayList<Integer> topWeekDays;
    ArrayList<Classement> topServedAirports;
}
