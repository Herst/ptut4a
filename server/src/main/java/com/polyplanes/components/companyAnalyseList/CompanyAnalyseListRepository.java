package com.polyplanes.components.companyAnalyseList;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CompanyAnalyseListRepository extends MongoRepository<CompanyAnalyseList, String> {
    CompanyAnalyseList findCompanyAnalyseListByIdCarrier(String id);
}
