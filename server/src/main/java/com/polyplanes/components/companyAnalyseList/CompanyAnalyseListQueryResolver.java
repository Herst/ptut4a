package com.polyplanes.components.companyAnalyseList;

import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CompanyAnalyseListQueryResolver implements GraphQLQueryResolver {
    @Autowired
    public CompanyAnalyseListService companyAnalyseListService;

    public CompanyAnalyseList companyAnalyseList(String id) {
        return this.companyAnalyseListService.getCompanyAnalyseListByCompanyId(id);
    }

    public List<CompanyAnalyseList> companyAnalyseLists() {
        return this.companyAnalyseListService.getAll();
    }
}
