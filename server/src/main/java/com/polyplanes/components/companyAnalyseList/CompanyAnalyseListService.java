package com.polyplanes.components.companyAnalyseList;

import com.polyplanes.components.Classement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyAnalyseListService {
    @Autowired
    public CompanyAnalyseListRepository companyAnalyseListRepository;

    public CompanyAnalyseList getCompanyAnalyseListByCompanyId(String id) {
        CompanyAnalyseList companyAnalyseList =
                this.companyAnalyseListRepository.findCompanyAnalyseListByIdCarrier(id);
        companyAnalyseList.getDATA().forEach(data -> {
            data.setAvgDelayedCanceledFlights(computeRealCancelLateRate(data));
            data.setTopServedAirports(getTop10ServedAirports(data.getTopServedAirports()));
        });
        return companyAnalyseList;
    }

    public List<CompanyAnalyseList> getAll() {
        List<CompanyAnalyseList> analyzes = this.companyAnalyseListRepository.findAll();
        analyzes.forEach(analyze -> analyze.getDATA().forEach(data -> data.setAvgDelayedCanceledFlights(computeRealCancelLateRate(data))));
        return analyzes;
    }

    private ArrayList<Classement> getTop10ServedAirports(ArrayList<Classement> topServedAirports) {
        if (topServedAirports.size() <= 10) {
            return topServedAirports;
        }

        return topServedAirports.stream()
                .sorted(Comparator.comparing(classement -> -1 * classement.getValue()))
                .limit(10).collect(Collectors.toCollection(ArrayList::new));
    }

    private float computeRealCancelLateRate(CompanyAnalyse companyAnalyse) {
        return 100 * ((companyAnalyse.getNbCanceledFlights()
                + companyAnalyse.getNbDelayedFlights()) / (float) companyAnalyse.getNbFlights());
    }
}
