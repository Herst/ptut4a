package com.polyplanes.components.airportAnalyseList;

import com.polyplanes.components.Classement;
import lombok.*;

import java.util.ArrayList;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AirportAnalyse {
    Integer nbCanceledFlights;
    Integer nbDelayedFlights;
    Float avgDelayedCanceledFlights;
    Integer nbFlights;
    //tps de vol moyen
    Float avgElapsedTime;
    //Distribution de la moyenne de temps de vol (<1h; 1h-3h30; >3h30)
    ArrayList<Integer> topByCatElapsedTime;
    //<Code de retard, nb iteration>
    ArrayList<Classement> topDelayReason;
    //Distribtuion pour l'id de l'aeroport <Company,% de vol qui entre et sorte>
    ArrayList<Classement> topCompanies;
    //Distribtuion pour l'id de l'aeroport <CancelReason,% de vol qui entre et sorte>
    ArrayList<Classement> topCancelReason;
    //Distribution du <Jour, nb vols entrants / sortants>
    ArrayList<Integer> topWeekDays;
    //Distribution pour un aeroport <Aeroport le plus desservi, nb de vols sortant>
    ArrayList<Classement> topServedAirports;
}
