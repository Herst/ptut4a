package com.polyplanes.components.airportAnalyseList;

import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AirportAnalyseListQueryResolver implements GraphQLQueryResolver {
    @Autowired
    public AirportAnalyseListService airportAnalyseListService;

    public AirportAnalyseList airportAnalyseList(String airportId) {
        return this.airportAnalyseListService.getAirportAnalyseListByAirportId(airportId);
    }

    public List<AirportAnalyseList> airportAnalyseLists() {
        return this.airportAnalyseListService.getAllAirportAnalyseList();
    }

    public long countAllAirports() {
        return this.airportAnalyseListService.countAllAirports();
    }
}
