package com.polyplanes.components.airportAnalyseList;

import com.polyplanes.components.Classement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AirportAnalyseListService {
    @Autowired
    public AirportAnalyseListRepository airportAnalyseListRepository;

    //Comme il n'y aura qu'une seule analyse on récupère le premier de la liste
    public AirportAnalyseList getAirportAnalyseListByAirportId(String airportId) {
        AirportAnalyseList airportAnalyseList =
                this.airportAnalyseListRepository.findAirportAnalyseListByIdAirport(airportId);
        airportAnalyseList.getDATA().forEach(data -> {
            data.setAvgDelayedCanceledFlights(computeRealCancelLateRate(data));
            data.setTopServedAirports(getTop10ServedAirports(data.getTopServedAirports()));
        });
        return airportAnalyseList;
    }

    public List<AirportAnalyseList> getAllAirportAnalyseList() {
        List<AirportAnalyseList> analyzes = this.airportAnalyseListRepository.findAll();
        analyzes.forEach(analyze -> analyze.getDATA().forEach(data -> data.setAvgDelayedCanceledFlights(computeRealCancelLateRate(data))));
        return analyzes;
    }

    public long countAllAirports() {
        return this.airportAnalyseListRepository.count();
    }

    private ArrayList<Classement> getTop10ServedAirports(ArrayList<Classement> topServedAirports) {
        if (topServedAirports.size() <= 10) {
            return topServedAirports;
        }

        return topServedAirports.stream()
                .sorted(Comparator.comparing(classement -> -1 * classement.getValue()))
                .limit(10).collect(Collectors.toCollection(ArrayList::new));
    }

    private float computeRealCancelLateRate(AirportAnalyse airportAnalyse) {
        return 100 * ((airportAnalyse.getNbCanceledFlights()
                + airportAnalyse.getNbDelayedFlights()) / (float) airportAnalyse.getNbFlights());
    }
}
