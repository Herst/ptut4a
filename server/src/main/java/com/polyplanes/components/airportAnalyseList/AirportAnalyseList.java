package com.polyplanes.components.airportAnalyseList;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.ArrayList;

@Document(collection = "airports")
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AirportAnalyseList {
    @Id
    String id;
    ArrayList<AirportAnalyse> DATA;
    String idAirport;
    String name;
}
