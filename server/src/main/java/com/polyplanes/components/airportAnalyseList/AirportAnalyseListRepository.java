package com.polyplanes.components.airportAnalyseList;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface AirportAnalyseListRepository extends MongoRepository<AirportAnalyseList, String> {
    AirportAnalyseList findAirportAnalyseListByIdAirport(String airportId);
}
