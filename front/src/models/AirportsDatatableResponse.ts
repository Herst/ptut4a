import {AirportAnalyzeList} from "../app/dashboard/airport-analyze-list";

export interface AirportsDatatableResponse{
  airportAnalyseLists : AirportAnalyzeList[];

}
