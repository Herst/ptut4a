export interface AnalyzedCard {
  title: string,
  img: string,
  value: number,
  percentage: number
}
