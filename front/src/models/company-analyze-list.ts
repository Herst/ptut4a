import {AnalyzedCompany} from "./analyzed-company";

export interface CompanyAnalyzeList {
  id: string,
  name: string,
  DATA: AnalyzedCompany[],
  idCarrier: string,
}
