export interface AnalysedCompany {
  id: string,
  name: string,
  canceledFligth: number,
  lateFligth: number,
  numberFligth: number,
  avgDuration: number,
  reliability: number
}
