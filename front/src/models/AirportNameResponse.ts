import {AirportNameId} from "./airport-name-id";

export interface AirportNameResponse{
  airportAnalyseLists : AirportNameId[];
}
