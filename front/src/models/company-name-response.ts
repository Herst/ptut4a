import {CompanyNameId} from "./company-name-id";

export interface CompanyNameResponse {
  companyAnalyseLists : CompanyNameId[];

}
