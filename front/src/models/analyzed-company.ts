import {StringIntegerMap} from "../app/dashboard/stacked-bar-chart/string-integer-map";

export interface AnalyzedCompany {
  name: string,
  idCarrier: string,
  nbCanceledFlights: number,
  nbFlights: number,
  nbDelayedFlights: number,
  avgDelayedCanceledFlights: number,
  topDelayReason: StringIntegerMap[],
  topCancelReason: StringIntegerMap[],
  avgElapsedTime: number
  topByCatElapsedTime: number[],
  topWeekDays: number[],
  topServedAirports: StringIntegerMap[]
}
