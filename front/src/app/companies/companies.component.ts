import {Component, OnInit} from '@angular/core';
import {AnalyzedCompany} from "../../models/analyzed-company";
import {CompanyService} from "../service/company.service";
import {CompanyAnalyzeList} from "../../models/company-analyze-list";
import {Router} from "@angular/router";

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {

  monthsValue = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin'];
  months: AnalyzedCompany[] = [];

  companies: CompanyAnalyzeList[] = [];
  loading = true;

  constructor(private companyService: CompanyService,
              private router: Router) { }

  ngOnInit(): void {
    this.companyService.getDataForDataTable().subscribe(values => {
      this.companies = values.data.companyAnalyseLists;
      this.map(0);
      this.loading = false;
    });
  }

  private map(dayIndex: number): void {
    this.months = this.companies
      .filter(company => company.DATA[dayIndex] !== null)
      .map(company => {
        return {
          ...company.DATA[dayIndex],
          name: company.name,
          idCarrier: company.idCarrier
        }
      });
  }

  onSelectMonth(m: string) {
    let index = this.monthsValue.indexOf(m);
    this.map(index);
  }

  onRowSelected(analyzedCompany: AnalyzedCompany) {
    this.router.navigate(['/dashboard/company'],
      {
        queryParams: {companyId: analyzedCompany.idCarrier}
      });
  }

}
