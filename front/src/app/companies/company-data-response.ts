import {CompanyAnalyzeList} from "../../models/company-analyze-list";

export interface CompanyDataResponse {
  companyAnalyseLists : CompanyAnalyzeList[];

}
