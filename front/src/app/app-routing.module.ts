import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HomePageComponent} from "./home-page/home-page.component";
import {AirlineComparisonComponent} from "./airline-comparison/airline-comparison.component";
import {AirportDashboardComponent} from "./dashboard/airport-dashboard/airport-dashboard.component";
import {AirlinesComponent} from "./airlines/airlines.component";
import {CompaniesComponent} from "./companies/companies.component";
import {CompanyDashboardComponent} from "./dashboard/company-dashboard/company-dashboard.component";


const routes: Routes = [
  {path: '', redirectTo: 'companies', pathMatch: 'full'},
  {path: 'home', component: HomePageComponent},
  {path: 'airline-comparison', component: AirlineComparisonComponent},
  {path: 'dashboard/airport', component: AirportDashboardComponent},
  {path: 'dashboard/company', component: CompanyDashboardComponent},
  {path: 'airports', component: AirlinesComponent},
  {path: 'companies', component: CompaniesComponent},
  {path: '**', redirectTo: 'companies', pathMatch: 'full'}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
