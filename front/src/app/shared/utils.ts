import {StringIntegerMap} from "../dashboard/stacked-bar-chart/string-integer-map";
import {AnalyzedCompany} from "../../models/analyzed-company";
import {AnalysedAirport} from "../dashboard/analysed-airport";

export const DAYS = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
export const FLIGHT_TIME = ['Vols < 1 heure', '1h <= Vols <= 3h30', 'Vols => 3h30', 'Vols annulés'];

export class Utils {

  public static round(value: number, decimalNumber: number): number {
    return Math.round((value + Number.EPSILON) * Math.pow(10 ,decimalNumber)) / Math.pow(10, decimalNumber);
  }

  public static mapWeekDays(weekDays: number[]): StringIntegerMap[] {
    const weekDaysMap: StringIntegerMap[] = [];
    for (let i = 0; i < DAYS.length; i++) {
      weekDaysMap.push({name: DAYS[i], value: weekDays[i]});
    }
    return weekDaysMap;
  }

  public static mapTopByCatElapsedTime(topByCatElapsedTime: number[]): StringIntegerMap[] {
    const topByCatElapsedTimeMap: StringIntegerMap[] = [];
    for (let i = 0; i < FLIGHT_TIME.length; i++) {
      topByCatElapsedTimeMap.push({name: FLIGHT_TIME[i], value: topByCatElapsedTime[i]});
    }
    return topByCatElapsedTimeMap;
  }

  public static computeDataForLinesChart(analyzes: AnalysedAirport[] | AnalyzedCompany[]) {
    const cancelFlights: number[] = [];
    const lateFlights: number[] = [];
    const lateCancelRate: number[] = [];
    const totalFlights: number[] = [];
    analyzes.forEach(analyze => {
      cancelFlights.push(analyze.nbCanceledFlights);
      lateFlights.push(analyze.nbDelayedFlights);
      lateCancelRate.push(analyze.avgDelayedCanceledFlights);
      totalFlights.push(analyze.nbFlights);
    });

    return {
      totalFlights,
      lateCancelRate,
      lateFlights,
      cancelFlights
    };
  }
}
