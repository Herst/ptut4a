import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  items: MenuItem[] = [];
  constructor() { }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Dashboard',
        icon: 'pi pi-fw pi-chart-bar',
        items: [{
          label: 'Compagnies',
          url: 'dashboard/company',
        },
          {
            label: 'Aéroports',
            url :'dashboard/airport'
          }]
      },
      {
        label: 'Nos données',
        icon : 'pi pi-fw  pi-chart-line',
        items: [{
          label: 'Compagnies',
          url: 'companies'
        },
          {
            label: 'Aéroports',
            url: 'airports'
          },
        ]
      },
    ];
  }

}
