import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {APOLLO_OPTIONS} from 'apollo-angular';
import {HttpLink} from 'apollo-angular/http';
import {InMemoryCache} from '@apollo/client/core';
import { NavbarComponent } from './navbar/navbar.component';
import {MenubarModule} from "primeng/menubar";

import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from '@angular/common/http';
import { HomePageComponent } from './home-page/home-page.component';
import { HomeButtonsComponent } from './home-page/home-buttons/home-buttons.component';
import { AirlineComparisonComponent } from './airline-comparison/airline-comparison.component';
import {AppRoutingModule} from "./app-routing.module";
import {DropdownModule} from "primeng/dropdown";
import { ComparisonTableComponent } from './airline-comparison/comparison-table/comparison-table.component';
import { AirlinesComponent } from './airlines/airlines.component';
import { DatatableComponent } from './airlines/datatable/datatable.component';
import {TableModule} from "primeng/table";
import { StackedBarChart } from './dashboard/stacked-bar-chart/stacked-bar-chart.component';
import {ChartModule} from "primeng/chart";
import { CustomCardComponent } from './dashboard/custom-card/custom-card.component';
import {CardModule} from "primeng/card";
import {AirportDashboardComponent} from "./dashboard/airport-dashboard/airport-dashboard.component";
import { AirportStatsComponent } from './dashboard/airport-dashboard/airport-stats/airport-stats.component';
import { DurationPipe } from './airlines/datatable/duration.pipe';
import { RoundPipe } from './airlines/datatable/round.pipe';
import { CompaniesComponent } from './companies/companies.component';
import {CompanyDashboardComponent} from "./dashboard/company-dashboard/company-dashboard.component";
import {CompanyStatsComponent} from "./dashboard/company-dashboard/company-stats/company-stats.component";
import { LineChartComponent } from './dashboard/line-chart/line-chart.component';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HomeButtonsComponent,
    NavbarComponent,
    AirlineComparisonComponent,
    ComparisonTableComponent,
    NavbarComponent,
    AirlinesComponent,
    DatatableComponent,
    CustomCardComponent,
    AirportDashboardComponent,
    StackedBarChart,
    CustomCardComponent,
    AirportStatsComponent,
    DurationPipe,
    RoundPipe,
    CompaniesComponent,
    CompanyDashboardComponent,
    CompanyStatsComponent,
    LineChartComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    MenubarModule,
    TableModule,
    MenubarModule,
    AppRoutingModule,
    DropdownModule,
    CardModule,
    DropdownModule,
    ChartModule,
    CardModule,
  ],
  providers: [
    {
      provide : APOLLO_OPTIONS,
      useFactory: (httpLink :HttpLink ) => {
        return {
          cache : new InMemoryCache(),
          link : httpLink.create({
            uri : 'http://localhost:4242/graphql',
          }),
        };
      },
      deps : [HttpLink],
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
