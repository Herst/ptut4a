import {Component, Input, OnChanges} from '@angular/core';
import {Utils} from "../../shared/utils";

@Component({
  selector: 'app-custom-card',
  templateUrl: './custom-card.component.html',
  styleUrls: ['./custom-card.component.scss']
})
export class CustomCardComponent implements OnChanges {

  @Input() value = 0;
  @Input() img = '';
  @Input() percentageBase = 0;
  @Input() title = '';
  @Input() iconName = 'pie_chart';
  @Input() percentageValue = false;

  computedPercentage?: number;

  ngOnChanges(): void {
    if (this.percentageBase) {
      this.computePercentage(2);
    }
  }

  private computePercentage(decimal: number): void {
    const percentage = (this.value / this.percentageBase) * 100;
    this.computedPercentage = Utils.round(percentage, decimal);
  }
}
