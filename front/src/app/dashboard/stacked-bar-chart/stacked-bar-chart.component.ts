import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {StringIntegerMap} from "./string-integer-map";

@Component({
  selector: 'app-chart',
  templateUrl: './stacked-bar-chart.component.html',
  styleUrls: ['./stacked-bar-chart.component.scss']
})
export class StackedBarChart implements OnInit, OnChanges {

  @Input() statsByMonth: StringIntegerMap[][] = [];
  @Input() title = 'Titre';

  data: any;
  options: any;
  colors = ['#ff4444', '#ff00cc', '#bf00ff', '#9900ff',
    '#5500ff', '#1900ff', '#009dff', '#00e1ff', '#9900ff',
    '#00ffc4', '#11ff00', '#a2ff00', '#eeff00',
    '#ffc400', '#ff7300', '#9b0000', '#570000'];

  cancelationsReasonsColors: Map<string, string>;
  lateColors: Map<string, string>;

  constructor() {
    this.cancelationsReasonsColors = new Map<string, string>([
      ['Weather', this.colors[0]],
      ['National Air System', this.colors[1]],
      ['Carrier', this.colors[2]],
      ['Security', this.colors[3]]
    ]);

    this.lateColors = new Map<string, string>([
      ['Avance < -15 minutes', this.colors[8]],
      ['Avance entre -15 et -1 minutes', this.colors[9]],
      ['Retard entre 0 et 14 minutes', this.colors[10]],
      ['Retard entre 15 et 29 minutes' , this.colors[11]],
      ['Retard entre 30 et 44 minutes', this.colors[12]],
      ['Retard entre 45 et 59 minutes', this.colors[13]],
      ['Retard entre 45 et 59 minutes', this.colors[1]],
      ['Retard entre 60 et 74 minutes', this.colors[2]],
      ['Retard entre 75 et 89 minutes', this.colors[3]],
      ['Retard entre 90 et 104 minutes', this.colors[4]],
      ['Retard entre 105 et 119 minutes', this.colors[5]],
      ['Retard entre 120 et 134 minutes', this.colors[6]],
      ['Retard entre 135 et 149 minutes', this.colors[7]],
      ['Retard entre 150 et 164 minutes', this.colors[14]],
      ['Retard entre 165 et 179 minutes', this.colors[15]],
      ['Retard >= 180 minutes', '#000000'],

    ])
  }

  ngOnInit(): void {
    this.initOptions();
    new Map<string, string>([

    ])
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initData();
  }

  private initOptions(): void {
    this.options = {
      plugins: {
        tooltips: {
          mode: 'index',
          intersect: false
        },
      },
      scales: {
        x: {
          stacked: true,
          ticks: {
            color: '#495057'
          },
          grid: {
            color: '#ebedef'
          }
        },
        y: {
          stacked: true,
          ticks: {
            color: '#495057'
          },
          grid: {
            color: '#ebedef'
          }
        }
      }
    };
  }

  private initData() {
    this.data = {
      labels: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin'],
      datasets: this.buildDataSets()
    };
  }

  private buildDataSets(): any[] {
    const numberOfKeys = this.getNumberOfKeys();
    const dataSets: any[] = [];
    for (let i = 0; i < numberOfKeys; i++) {
      if (!!this?.statsByMonth[0][i]?.name) {
        const name = this.statsByMonth[0][i].name;
        dataSets.push({
          type: 'bar',
          label: name,
          data: this.buildDatas(i),
          backgroundColor: this.getColor(i, name)
        });
      }
    }
    return dataSets;
  }

  private buildDatas(keyIndex: number): number[] {
    const datas: number[] = [];
    this.statsByMonth.forEach(stat => {
      if (!!stat[keyIndex]) {
        datas.push(stat[keyIndex].value);
      }
    })
    return datas;
  }

  private getNumberOfKeys(): number {
    let max = this.statsByMonth[0].length;
    for(let i = 1 ; i < this.statsByMonth.length; i++) {
      if (this.statsByMonth[i].length > max) {
        max = this.statsByMonth[i].length;
      }
    }
    return max;
  }

  private getColor(index: number, name: string): string {
    if (this.title === 'Annulations') {
      return this.cancelationsReasonsColors.get(name)!!;
    } else if (this.title === 'Retards') {
      return this.lateColors.get(name)!!;
    } else if (this.title === 'Temps de vols') {
      if (index === 3) {
        return '#000000';
      }
      return this.colors[index];
    } else {
      return this.colors[index];
    }
  }
}
