import {Component, Input, OnChanges, OnInit} from '@angular/core';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit, OnChanges {

  @Input() datas: number[] = [];
  @Input() label: string = '';

  colors = ['#ff5757', '#446fff', '#58ff89', '#ffec6a']

  chartData: any;
  chartOptions: any;

  constructor() { }

  ngOnInit(): void {
    this.initOptions();
  }

  ngOnChanges() {
    this.initChartData();
  }

  private initChartData() {
    this.chartData = {
      labels: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin'],
      datasets: [
        {
          label: this.label,
          data: this.datas,
          fill: false,
          borderColor: this.getBorderColors(),
          tension : .4
        }
      ]
    }
  }

  private initOptions() {
    this.chartOptions = {
      plugins: {
        legend: {
          labels: {
            color: '#495057'
          }
        }
      },
      scales: {
        x: {
          ticks: {
            color: '#495057'
          },
          grid: {
            color: '#ebedef'
          }
        },
        y: {
          ticks: {
            color: '#495057'
          },
          grid: {
            color: '#ebedef'
          }
        }
      }
    };
  }

  private getBorderColors(): string {
    switch (this.label) {
      case 'Vols annulés': return this.colors[0];
      case 'Vols en retard': return this.colors[1];
      case '% vols annulés/retardés': return this.colors[2];
      default: return this.colors[3];
    }
  }
}
