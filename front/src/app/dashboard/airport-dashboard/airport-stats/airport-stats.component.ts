import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {AnalysedAirport} from "../../analysed-airport";
import {StringIntegerMap} from "../../stacked-bar-chart/string-integer-map";
import {Utils} from "../../../shared/utils";

@Component({
  selector: 'app-airport-stats',
  templateUrl: './airport-stats.component.html',
  styleUrls: ['./airport-stats.component.scss']
})
export class AirportStatsComponent implements OnChanges {

  @Input() analysedAirports: AnalysedAirport[] = [];
  @Input() currentAnalyze?: AnalysedAirport;

  statsByMonth: {
    topByCatElapsedTime: StringIntegerMap[][],
    topDelayReason: StringIntegerMap[][],
    topComanies: StringIntegerMap[][],
    topWeekDays: StringIntegerMap[][],
    topCancelReason: StringIntegerMap[][],
    topServedAirports: StringIntegerMap[][]
  };

  statsForLines: {
    cancelFlights: number[],
    lateFlights: number[],
    lateCancelRate: number[],
    totalFlights: number[]
  };

  constructor() {
    this.statsByMonth = {
      topByCatElapsedTime: [],
      topDelayReason: [],
      topWeekDays: [],
      topComanies: [],
      topCancelReason: [],
      topServedAirports: []
    };

    this.statsForLines = {
      cancelFlights: [],
      lateCancelRate: [],
      lateFlights: [],
      totalFlights: []
    };
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.analysedAirports) {
      this.buildStatsByMonths();
      this.statsForLines = Utils.computeDataForLinesChart(this.analysedAirports);
    }
  }

  private buildStatsByMonths(): void {
    const topByCatElapsedTime: StringIntegerMap[][] = [];
    const topDelayReason: StringIntegerMap[][] = [];
    const topComanies: StringIntegerMap[][] = [];
    const topWeekDays: StringIntegerMap[][] = [];
    const topCancelReason: StringIntegerMap[][] = [];
    const topServedAirports: StringIntegerMap[][] = [];
    this.analysedAirports.forEach(analyzedAirport => {
      topByCatElapsedTime.push(Utils.mapTopByCatElapsedTime(analyzedAirport.topByCatElapsedTime));
      topDelayReason.push(analyzedAirport.topDelayReason);
      topComanies.push(analyzedAirport.topCompanies);
      topWeekDays.push(Utils.mapWeekDays(analyzedAirport.topWeekDays));
      topCancelReason.push(analyzedAirport.topCancelReason);
      topServedAirports.push(analyzedAirport.topServedAirports);
    });
    this.statsByMonth = {
      topComanies,
      topByCatElapsedTime,
      topDelayReason,
      topWeekDays,
      topCancelReason,
      topServedAirports
    }
  }

}
