import {Component, OnInit} from '@angular/core';
import {AnalysedAirport} from "../analysed-airport";
import {AnalyzedAirportService} from "../../service/analyzed-airport.service";
import {ActivatedRoute} from "@angular/router";
import {AirportNameId} from "../../../models/airport-name-id";

@Component({
  selector: 'app-dashboard',
  templateUrl: './airport-dashboard.component.html',
  styleUrls: ['./airport-dashboard.component.scss']
})
export class AirportDashboardComponent implements OnInit {

  airportNames: AirportNameId[] = [];

  analyzedAirports: AnalysedAirport[] = [];
  analyzedAirportsToCompare: AnalysedAirport[] = [];

  currentAnalyze?: AnalysedAirport;
  currentAnalyzeToCompare?: AnalysedAirport;

  selectedAirportId: string | null = '';
  selectedComparedAirportId: string = '';

  months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin'];
  currentMonth = 'Janvier';

  constructor(private analyzedAirportService: AnalyzedAirportService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.analyzedAirportService.getNameAirports().subscribe((value) => {
      this.airportNames = value.data.airportAnalyseLists;
      const airportIdParam = this.route.snapshot.queryParamMap.get('airportId');
      this.selectedAirportId = !!airportIdParam ? airportIdParam : this.airportNames[0].idAirport;
      this.fetchAnalyzedAirports(this.selectedAirportId);
    });
  }


  private fetchAnalyzedAirports(airportId: string, airportToCompare = false): void {
    this.analyzedAirportService.getAnalyzedAirport(airportId)
      .subscribe(result => {
        if (airportToCompare) {
          this.analyzedAirportsToCompare = result.data.airportAnalyseList.DATA
        } else {
          this.analyzedAirports = result.data.airportAnalyseList.DATA;
        }
        this.updateCurrentAnalyze();
      });
  }

  updateCurrentAnalyze(): void {
    if (this.analyzedAirportsToCompare.length > 0) {
      this.currentAnalyzeToCompare = this.analyzedAirportsToCompare[this.months.indexOf(this.currentMonth)];
    }
    this.currentAnalyze = this.analyzedAirports[this.months.indexOf(this.currentMonth)];
  }

  updateDataAirport(id: string, airportToCompare = false) {
    this.fetchAnalyzedAirports(id, airportToCompare);
  }
}
