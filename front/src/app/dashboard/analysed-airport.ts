import {StringIntegerMap} from "./stacked-bar-chart/string-integer-map";

export interface AnalysedAirport {
  idAirport: string,
  name: string,
  nbCanceledFlights: number,
  nbDelayedFlights: number,
  avgDelayedCanceledFlights: number,
  nbFlights: number,
  avgElapsedTime : number,
  topByCatElapsedTime: number[],
  topDelayReason: StringIntegerMap[],
  topCancelReason: StringIntegerMap[],
  topCompanies: StringIntegerMap[],
  topWeekDays: number[],
  topServedAirports: StringIntegerMap[]
}
