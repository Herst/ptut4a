import {AnalysedAirport} from "./analysed-airport";

export interface AirportAnalyzeList {
  id: string,
  name: string,
  DATA: AnalysedAirport[],
  idAirport: string,
}
