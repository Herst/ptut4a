import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {StringIntegerMap} from "../../stacked-bar-chart/string-integer-map";
import {AnalyzedCompany} from "../../../../models/analyzed-company";
import {Utils} from "../../../shared/utils";

@Component({
  selector: 'app-company-stats',
  templateUrl: './company-stats.component.html',
  styleUrls: ['./company-stats.component.scss']
})
export class CompanyStatsComponent implements OnChanges {

  @Input() analyzedCompanies: AnalyzedCompany[] = [];
  @Input() currentAnalyze?: AnalyzedCompany;

  statsByMonth: {
    topByCatElapsedTime: StringIntegerMap[][],
    topDelayReason: StringIntegerMap[][],
    topWeekDays: StringIntegerMap[][],
    topCancelReason: StringIntegerMap[][],
    topServedAirports: StringIntegerMap[][]
  };

  statsForLines: {
    cancelFlights: number[],
    lateFlights: number[],
    lateCancelRate: number[],
    totalFlights: number[]
  };

  constructor() {
    this.statsByMonth = {
      topByCatElapsedTime: [],
      topDelayReason: [],
      topWeekDays: [],
      topCancelReason: [],
      topServedAirports: []
    };

    this.statsForLines = {
      cancelFlights: [],
      lateCancelRate: [],
      lateFlights: [],
      totalFlights: []
    };
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.analyzedCompanies) {
      this.buildStatsByMonths();
      this.statsForLines = Utils.computeDataForLinesChart(this.analyzedCompanies);
    }
  }

  private buildStatsByMonths(): void {
    const topByCatElapsedTime: StringIntegerMap[][] = [];
    const topDelayReason: StringIntegerMap[][] = [];
    const topWeekDays: StringIntegerMap[][] = [];
    const topCancelReason: StringIntegerMap[][] = [];
    const topServedAirports: StringIntegerMap[][] = [];
    this.analyzedCompanies.forEach(analyzedAirport => {
      topByCatElapsedTime.push(Utils.mapTopByCatElapsedTime(analyzedAirport.topByCatElapsedTime));
      topDelayReason.push(analyzedAirport.topDelayReason);
      topWeekDays.push(Utils.mapWeekDays(analyzedAirport.topWeekDays));
      topCancelReason.push(analyzedAirport.topCancelReason);
      topServedAirports.push(analyzedAirport.topServedAirports);
    });
    this.statsByMonth = {
      topByCatElapsedTime,
      topDelayReason,
      topWeekDays,
      topCancelReason,
      topServedAirports
    }
  }
}
