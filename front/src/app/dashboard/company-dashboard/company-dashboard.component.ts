import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CompanyService} from "../../service/company.service";
import {CompanyNameId} from "../../../models/company-name-id";
import {AnalyzedCompany} from "../../../models/analyzed-company";

@Component({
  selector: 'app-company-dashboard',
  templateUrl: './company-dashboard.component.html',
  styleUrls: ['./company-dashboard.component.scss']
})
export class CompanyDashboardComponent implements OnInit {

  companyNames: CompanyNameId[] = [];

  analyzedCompany: AnalyzedCompany[] = [];
  analyzedCompanyToCompare: AnalyzedCompany[] = [];

  currentAnalyze?: AnalyzedCompany;
  currentAnalyzeToCompare?: AnalyzedCompany;

  selectedCompanyId: string | null = '';
  selectedComparedCompanyId: string = '';

  months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin'];
  currentMonth = 'Janvier';

  constructor(private companyService: CompanyService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.companyService.getNameCompanies().subscribe((value) => {
      this.companyNames = value.data.companyAnalyseLists;
      const carrierIdParam = this.route.snapshot.queryParamMap.get('companyId');
      this.selectedCompanyId = !!carrierIdParam ? carrierIdParam : this.companyNames[0].idCarrier;
      this.fetchAnalyzedAirports(this.selectedCompanyId);
    });
  }


  private fetchAnalyzedAirports(companyId: string, companyToCompare = false): void {
    this.companyService.getAnalyzedCompany(companyId)
      .subscribe(result => {
        if (companyToCompare) {
          this.analyzedCompanyToCompare = result.data.companyAnalyseList.DATA
        } else {
          this.analyzedCompany = result.data.companyAnalyseList.DATA;
        }
        this.updateCurrentAnalyze();
      });
  }

  updateCurrentAnalyze(): void {
    if (this.analyzedCompanyToCompare.length > 0) {
      this.currentAnalyzeToCompare = this.analyzedCompanyToCompare[this.months.indexOf(this.currentMonth)];
    }
    this.currentAnalyze = this.analyzedCompany[this.months.indexOf(this.currentMonth)];
  }

  updateDataCompany(id: string, airportToCompare = false) {
    this.fetchAnalyzedAirports(id, airportToCompare);
  }

}
