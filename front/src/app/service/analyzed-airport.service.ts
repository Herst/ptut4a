import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {Apollo} from "apollo-angular";
import {ApolloQueryResult, gql} from "@apollo/client/core";
import {AirportNameResponse} from "../../models/AirportNameResponse";
import {AnalysedAirport} from "../dashboard/analysed-airport";
import {AirportsDatatableResponse} from "../../models/AirportsDatatableResponse";
import {StringIntegerMap} from "../dashboard/stacked-bar-chart/string-integer-map";
import {AnalyzedAiportResponse} from "./analyzed-aiport-response";

@Injectable({
  providedIn: 'root'
})
export class AnalyzedAirportService {

  readonly GET_AIRPORT_ANALYSE = gql`
   query($idAirport: String!) {
    airportAnalyseList(idAirport: $idAirport) {
      id
      DATA {
        nbCanceledFlights
        nbDelayedFlights
        avgDelayedCanceledFlights
        nbFlights
        avgElapsedTime
        topByCatElapsedTime
        topDelayReason {
          name
          value
        }
        topCompanies {
          name
          value
        }
        topCancelReason {
          name
          value
        }
        topWeekDays
        topServedAirports {
          name
          value
        }
      }
    }
  }`;

  readonly GET_AIRPORT_DATATABLE = gql`
  query {
  airportAnalyseLists {
    name
    idAirport
    DATA {
      nbCanceledFlights
      nbDelayedFlights
      avgDelayedCanceledFlights
      nbFlights
      avgElapsedTime
    }
  }
}

 `;

  readonly GET_AIRPORTS_NAME = gql`
  query {
    airportAnalyseLists {
      name
      idAirport
    }
  } `;

  constructor(private apollo: Apollo) {
  }

  public getAnalyzedAirport(idAirport: string): Observable<ApolloQueryResult<AnalyzedAiportResponse>> {
    return this.apollo.watchQuery<AnalyzedAiportResponse>({
      query: this.GET_AIRPORT_ANALYSE,
      variables: {
        idAirport
      }
    }).valueChanges;
  }


  public getNameAirports(): Observable<ApolloQueryResult<AirportNameResponse>> {
    return this.apollo.watchQuery<AirportNameResponse>({
      query: this.GET_AIRPORTS_NAME,
    }).valueChanges;
  }

  public getAnalyzedAirportMocked(): Observable<AnalysedAirport[]> {
    return of(this.generateAnalyzedAirpots(6));
  }


  private generateAnalyzedAirpots(nb: number): AnalysedAirport[] {
    const airports: AnalysedAirport[] = [];
    for (let i = 0; i <= nb; i++) {
      airports.push({
        avgDelayedCanceledFlights: this.randomNumber(),
        nbFlights: this.randomNumber(),
        nbDelayedFlights: this.randomNumber(),
        nbCanceledFlights: this.randomNumber(),
        topWeekDays: [1,2,3,4,5,6,7],// this.generateKeyValues(7, 20, 200),
        topDelayReason: this.generateKeyValues(4, 20, 100),
        topCompanies: this.generateKeyValues(10, 20, 150),
        topByCatElapsedTime: [1,2,3,4,5],
        topServedAirports: this.generateKeyValues(10, 20, 100),
        topCancelReason: this.generateKeyValues(4, 20, 200),
        avgElapsedTime: this.randomNumber(),
        name: 'osef',
        idAirport: 'ac'
      })
    }
    return airports;
  }

  private generateKeyValues(nb: number, min: number, max: number): StringIntegerMap[] {
    const map: StringIntegerMap[] = [];
    const alphabet = 'ABCDEFGH';
    for (let i = 0; i <= nb; i++) {
      map.push({name: alphabet.charAt(i), value: this.randomNumber(min, max)});
    }
    return map;
  }

  private randomNumber(from = 50, to = 500) {
    return Math.round(Math.random() * (to - from) + to);
  }


  public getDataForDatatable(): Observable<ApolloQueryResult<AirportsDatatableResponse>> {
    return this.apollo.watchQuery<AirportsDatatableResponse>({
      query: this.GET_AIRPORT_DATATABLE,
    }).valueChanges;
  }


}
