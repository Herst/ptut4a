import {AirportAnalyzeList} from "../dashboard/airport-analyze-list";

export interface AnalyzedAiportResponse {
  id: string,
  airportAnalyseList: AirportAnalyzeList;
}
