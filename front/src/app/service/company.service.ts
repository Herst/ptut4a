import {Injectable} from '@angular/core';
import {Apollo} from "apollo-angular";
import {Observable} from "rxjs";
import {ApolloQueryResult, gql} from "@apollo/client/core";
import {CompanyDataResponse} from "../companies/company-data-response";
import {CompanyNameResponse} from "../../models/company-name-response";
import {AnalyzedCompanyResponse} from "./analyzed-company-response";

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  readonly COMPANY_DATA = gql`
    query {
    companyAnalyseLists {
      name
      idCarrier
      DATA {
        nbCanceledFlights
        nbDelayedFlights
        avgDelayedCanceledFlights
        nbFlights
        avgElapsedTime
      }
    }
  }
  `

  readonly GET_COMPANY_NAMES = gql`
    query {
    companyAnalyseLists {
      name
      idCarrier
    }
  }
  `

  readonly ANALYZE_COMPANIES = gql`
    query($idCarrier: String!) {
    companyAnalyseList(idCarrier: $idCarrier) {
      id
      DATA {
        nbCanceledFlights
        nbDelayedFlights
        avgDelayedCanceledFlights
        nbFlights
        avgElapsedTime
        topByCatElapsedTime
        topDelayReason {
          name
          value
        }
        topCancelReason {
          name
          value
        }
        topWeekDays
        topServedAirports {
          name
          value
        }
      }
    }
  }
  `
  constructor(private apollo: Apollo) {
  }

  public getDataForDataTable(): Observable<ApolloQueryResult<CompanyDataResponse>> {
    return this.apollo.watchQuery<CompanyDataResponse>({
      query: this.COMPANY_DATA
    }).valueChanges;
  }

  public getNameCompanies(): Observable<ApolloQueryResult<CompanyNameResponse>> {
    return this.apollo.watchQuery<CompanyNameResponse>({
      query: this.GET_COMPANY_NAMES
    }).valueChanges;
  }

  public getAnalyzedCompany(idCarrier: string): Observable<ApolloQueryResult<AnalyzedCompanyResponse>> {
    return this.apollo.watchQuery<AnalyzedCompanyResponse>({
      query: this.ANALYZE_COMPANIES,
      variables: {
        idCarrier
      }
    }).valueChanges;
  }
}
