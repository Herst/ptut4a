import {CompanyAnalyzeList} from "../../models/company-analyze-list";

export interface AnalyzedCompanyResponse {
  id: string,
  companyAnalyseList: CompanyAnalyzeList;

}
