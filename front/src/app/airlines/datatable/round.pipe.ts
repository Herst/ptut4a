import { Pipe, PipeTransform } from '@angular/core';
import {Utils} from "../../shared/utils";

@Pipe({
  name: 'round'
})
export class RoundPipe implements PipeTransform {

  transform(value: number, decimal: number): unknown {
    return Utils.round(value, decimal);
  }

}
