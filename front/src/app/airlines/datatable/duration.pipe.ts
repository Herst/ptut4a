import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(minutes: number, ...args: unknown[]): string {
    if (!minutes) {
      return '';
    }
    const rounded = Math.round(minutes);
    const hours = Math.floor(rounded / 60);
    const remainingMinutes = rounded % 60;
    if (!hours) {
      return remainingMinutes + 'm';
    } else {
      const minutesStr = remainingMinutes < 10 ? `0${remainingMinutes}` : remainingMinutes.toString();
      return `${hours}h${minutesStr}`
    }
  }

}
