import {Component, OnInit} from '@angular/core';
import {AnalyzedAirportService} from "../../service/analyzed-airport.service";
import {AnalysedAirport} from "../../dashboard/analysed-airport";
import {AirportAnalyzeList} from "../../dashboard/airport-analyze-list";
import {Router} from "@angular/router";


@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss']
})


export class DatatableComponent implements OnInit {

  airports: AirportAnalyzeList[] = [];
  months: AnalysedAirport[] = [];

  loading: boolean = false;
  monthsValue = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin'];
  selectedAirport?: AnalysedAirport;

  constructor(private analyzedAirportService: AnalyzedAirportService,
              private router: Router) {

  }

  ngOnInit(): void {
    this.initTable();
  }

  initTable() {
    this.loading = true;
    this.analyzedAirportService.getDataForDatatable().subscribe(values => {
      this.airports = values.data.airportAnalyseLists;
      this.map(0);
      this.loading = false;
    });
  }

  onSelectMonth(m: string) {
    let index = this.monthsValue.indexOf(m);
    this.map(index);
  }

  private map(dayIndex: number): void {
    this.months = this.airports
      .filter(airport => !!airport.DATA[dayIndex])
      .map(airport => {
        return {
          ...airport.DATA[dayIndex],
          name: airport.name,
          idAirport: airport.idAirport
        }
      });
  }

  onRowSelected(analyzeAirport: AnalysedAirport) {
    this.router.navigate(['/dashboard/airport'],
      {
        queryParams: {airportId: analyzeAirport.idAirport}
      });
  }
}


