export interface Airline {
  id: string,
  name: string,
  cancellationRate: number,
  delayRate: number,
  delayAverage: number
  flightNumbers: number

}
