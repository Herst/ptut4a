import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {of} from "rxjs";
import {AnalysedCompany} from "../../../models/analysed-company";

@Component({
  selector: 'app-comparison-table',
  templateUrl: './comparison-table.component.html',
  styleUrls: ['./comparison-table.component.scss']
})
export class ComparisonTableComponent implements OnInit {

  companies: (AnalysedCompany | undefined) [] = [];
  allCompanies: AnalysedCompany[] = [];
  airline1Id?: string;
  airline2Id?: string;

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.fetchAllAirlines();
    const airlineId1 = this.route.snapshot.queryParamMap.get('a1');
    const airlineId2 = this.route.snapshot.queryParamMap.get('a2');
    if (!!airlineId1) {
      this.fetchAirline(airlineId1, 0);
    }
    if (!!airlineId2) {
      this.fetchAirline(airlineId2, 1);
    }

  }

  private fetchAllAirlines() {
    const str = 'ABCDEFGHIJK';
    for (let i = 0; i < str.length; i++) {
      const name = str.charAt(i);
      this.allCompanies.push({
        name,
        canceledFligth: 78,
        lateFligth: 45,
        numberFligth: 78,
        avgDuration: 45,
        reliability: 0.8,
        id: name + 'id'
      } as AnalysedCompany);
    }
  }

  private fetchAirline(airlineId: string, index: number): void {
    of({
      id: airlineId,
      canceledFligth: 111,
      lateFligth: 222,
      numberFligth: 33,
      avgDuration: 44,
      reliability: 0.99,
      name: 'Company 1'
    } as AnalysedCompany)
      .subscribe(airline => {
        this.companies[index] = airline;
        if (index === 0) {
          this.airline1Id = airline.id;
        } else {
          this.airline2Id = airline.id;
        }
      });
  }

  onSelectAirline(event: { value: string; }, index: number) {
    this.companies[index] = this.allCompanies.find(airline => airline.id === event.value)
    const queryParams = this.buildQueryParams(index);
    this.router.navigate([],
      {
        relativeTo: this.route,
        queryParamsHandling: 'merge',
        queryParams
      })
  }

  buildQueryParams(index: number) {
    if (index === 0) {
      return {a1: this.companies[0]?.id};
    } else {
      return {a2: this.companies[1]?.id};
    }
  }
}
