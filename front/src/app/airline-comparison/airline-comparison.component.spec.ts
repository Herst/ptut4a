import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AirlineComparisonComponent } from './airline-comparison.component';

describe('AirlineComparisonComponent', () => {
  let component: AirlineComparisonComponent;
  let fixture: ComponentFixture<AirlineComparisonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AirlineComparisonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AirlineComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
